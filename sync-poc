#!/bin/sh

set -e
#set -x

if [ -n "${1}" ] ; then
	OCI_POC_HOST=${1}
else
	OCI_POC_HOST=bdbdev-ogiz
fi

echo "===========> Syncing PHP code <==========="
echo "===> Making tarball of PHP code"
cd src
tar -czf php.tar.gz *.php *.json inc
echo "===> Copying PHP code to ${OCI_POC_HOST}"
scp php.tar.gz root@${OCI_POC_HOST}:
rm php.tar.gz
cd ..
echo "===> Extracting PHP in OCI"
ssh -A root@${OCI_POC_HOST} "scp php.tar.gz oci:"
ssh -A root@${OCI_POC_HOST} "ssh oci 'cd /usr/share/openstack-cluster-installer ; tar -xzf /root/php.tar.gz ; rm /root/php.tar.gz'"
ssh root@${OCI_POC_HOST} "rm php.tar.gz"

echo "===> Synching db"
ssh -A root@${OCI_POC_HOST} "ssh oci 'php /usr/share/openstack-cluster-installer/db_sync.php'"

echo "===========> Syncing Puppet code <==========="
echo "===> Making Puppet manifests tarball"
cd puppet
tar -czf puppet.tar.gz lib manifests templates files
echo "===> Copying Puppet code to ${OCI_POC_HOST}"
scp puppet.tar.gz root@${OCI_POC_HOST}:
rm puppet.tar.gz
echo "===> Extracting Puppet code in OCI"
ssh -A root@${OCI_POC_HOST} "scp puppet.tar.gz oci:/usr/share/puppet/modules/oci && rm puppet.tar.gz"
ssh -A root@${OCI_POC_HOST} "ssh oci 'cd /usr/share/puppet/modules/oci ; tar -xzf puppet.tar.gz ; rm puppet.tar.gz ; /etc/init.d/puppet-master restart'"
cd ..

echo "===========> Syncing OCI shell scripts <==========="
echo "===> Making bin shell scripts tarball"
tar -czf bin.tar.gz bin
echo "===> Copying bin shell scripts to ${OCI_POC_HOST}"
scp bin.tar.gz root@${OCI_POC_HOST}:
rm bin.tar.gz
echo "===> Extracting bin shell scripts in OCI"
ssh -A root@${OCI_POC_HOST} "scp bin.tar.gz oci:"
ssh -A root@${OCI_POC_HOST} "ssh oci 'tar -xzf bin.tar.gz ; chown -R root:root bin ; chmod -R 755 bin ; cp bin/* /usr/bin ; rm -r bin bin.tar.gz ;' "
ssh -A root@${OCI_POC_HOST} "rm bin.tar.gz"

echo "===========> Syncing PoC shell scripts <==========="
echo "===> Copying PoC script shells to ${OCI_POC_HOST}"
tar -czf poc-bin.tar.gz poc-bin
scp poc-bin.tar.gz root@${OCI_POC_HOST}:
ssh root@${OCI_POC_HOST} "tar -xzf poc-bin.tar.gz ; cp poc-bin/* /usr/bin ; rm -fr poc-bin poc-bin.tar.gz"
rm poc-bin.tar.gz


echo "===========> Syncing ocicli and auto-complete <==========="
scp ocicli/ocicli root@${OCI_POC_HOST}:/usr/bin
scp ocicli/completions/ocicli root@${OCI_POC_HOST}:/usr/share/bash-completion/completions
scp src/variables.json root@${OCI_POC_HOST}:/etc/ocicli/
