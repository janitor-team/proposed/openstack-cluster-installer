.TH ocicli 1
.SH NAME
ocicli \- configure your baremetal cluster through the OCI API
.SH SYNOPSIS
.B ocicli
.B <command> [options]

.SH DESCRIPTION
.LP
The
.I ocicli
shell script configures the cluster OCI drives.

.SH "OUTPUT FORMAT"

.LP
.I \-csv
.IP
ocicli may be called with \-csv as first argument to display using a
csv output.

.SH "COMMANDS"

.LP
.B machine\-list
.I [-a|all]
.I [-v|--versions]
.I [-s|--status]
.I [-z|--zones]
.I [-r|--racking]
.I [-n|--network]
.I [-h|--hardware]
.I [-b|--blockdevs]
.I [-i|--ipmi]
.I [-l|--links]
.IP
List all the computers recorded in the OCI database. This command understand
a few flags as below. Each time, there is a shorter version of the option
using the first letter of the argument and a single minus sign.

.TP
.I \-\-all | \-a
Display all fields, as if all display options where passed. This usually
doesn't fit on a normal screen, but may be useful when parsing the output in CSV mode.

.TP
.I \-\-versions | \-v
Display BIOS, Dell Lifecycle and IPMI firmware versions.

.TP
.I \-\-status | \-s
Display the operating system installation and puppet run statuses. Also
display the time of the last run of openstack\-cluster\-installer\-agent. Note
that the agen only runs when the machine is running in Debian live, before
being installed.

.TP
.I \-\-zones | \-z
Display the cluster name, swift region and location where the machine has
been installed.

.TP
.I \-\-network | \-n
Display the networking information, such as IP addresses.

.TP
.I \-\-ethernet | \-e
Display the network interfaces with MAC addresses and speed and the switch
name and interface connected to it.

.TP
.I \-\-hardware | \-h
Display the hardware configuration of the machine, including the amount
of available memory, and the detected block and network devices.

.TP
.I \-\-blockdevs | \-b
Display the connected block devices.

.TP
.I \-\-ipmi | \-i
Display the IPMI information.

.TP
.I \-\-links | \-l
Display the network interfaces without MAC addresses and speed. This is
useful as the -e option can be very wide and not display well on smaller
screens.

.LP
.B machine\-show
.I <serial-num|hostname>
.IP
Show the details about a computer in the cluster.

.LP
.B machine\-guessed\-profile
.I <serial-num|hostname>
.I [\-d / \-\-debug]
.IP
Show the guessed hardware profile according to
/etc/openstack\-cluster\-installer/hardware-profiles.json. This is useful to
tweak hardware-profiles.json by trials and errors.

The \-d or \-\-debug option help designing hardware-profiles.json, by
showing how profiles are filtered.

.LP
.B machine\-check\-megacli\-applied
.I <serial\-num|hostname>
.I [hardware\-profile\-name]
.IP
Check if the megacli profile is applied or not. If no hardware profile is
given as parameter, it is then guessed.

.LP
.B machine\-megacli\-apply
.I <machine_serial>
.I [hardware_profile_name]"
.IP
Apply the MegaCli commands to a machine. If no hardware profile is given
as parameter, it is then guessed.

.LP
.B machine\-megacli\-reset\-raid
.I <machine_serial>
.IP
Reset the RAID configuration of a machine, removing all RAID devices.

.LP
.B machine\-guess\-racking
.I <machine_serial>
.IP
Print the datacenter, row, rack and position in the rack, as guessed by OCI,
reading /etc/openstack-cluster-installer/auto-racking.json.

.LP
.B machine\-auto\-rack
.I <machine_serial>
.IP
Fill\-up the racking information as machine-guess-racking would guess it.

.LP
.B machine\-auto\-add
.I <machine_serial>
.IP
Automatically add a machine to a cluster, guessing its role thanks to the
hardware profiles defined in
/etc/openstack\-cluster\-installer/hardware\-profiles.json (needed to guess
the machine role) and the racking infomation in
/etc/openstack-cluster-installer/auto-racking.json (needed to guess the
machine location name).

.LP
.B machine\-to\-dns
.I <machine_serial>
.IP
Record the machine hostname into an external DNS service. This works only
if the [dns_plugin] section of openstack\-cluster\-installer.conf is configured
properly. See the OCI contrib folder in the source of OCI to see how to
write the plugin shell script.

.LP
.B machine\-out\-of\-dns
.I <machine_serial>
.IP
Delete the machine from DNS using the plugin script.

.LP
.B machine\-to\-monitoring
.I <machine_serial>
.IP
Insert the machine into the monitoring system. This works only
if the [monitoring_plugin] section of openstack\-cluster\-installer.conf is configured
properly. See the OCI contrib folder in the source of OCI to see how to
write the plugin shell script.

.LP
.B machine\-out\-of\-monitoring
.I <machine_serial>
.IP
Delete the machine from monitoring using the plugin script.

.LP
.B machine\-gen\-root\-pass
.I <machine_serial>
.IP
Generate a random password, ssh into the machine, and set this password
for root. Optionally, if the [root_pass_plugin] section of
openstack\-cluster\-installer.conf is configured properly, record the root
password using the configured helper script. It is adviced to use something
like Hashicorp Vault or OpenStack Barbican for storing these root passwords.

.LP
.B machine\-forget\-root\-pass
.I <machine_serial>
.IP
Remove the root password from the password store, using the plugin script.

.LP
.B machine\-set
.I <serial\-num> [options]
.IP

Set properties of a computer in the cluster. Options are as below.

.TP
.I \-\-hostname <hostname>
Overrides the hostname as calculated by OCI when adding a computer in a
cluster. Must be issued before installing a server.

.TP
.I \-\-install\-on\-raid <yes/no>
Install the operating system using software RAID. See below on how to
configure such a RAID array.

.TP
.I \-\-raid\-type <0/1/10/5>
Select RAID type 0, 1, 10 or 5.

.TP
.I \-\-raid\-dev0 <sda>
Select sda for the RAID device 0.

.TP
.I \-\-raid\-dev1 <sda>
Select sda for the RAID device 1.

.TP
.I \-\-raid\-dev2 <sda>
Select sda for the RAID device 2.

.TP
.I \-\-raid\-dev3 <sda>
Select sda for the RAID device 3.

.TP
.I \-\-serial\-console\-device <ttyS0/ttyS1/none>
Select the serial console device for the main console output. This is very
useful if one is using IPMI and serial over LAN. This will influence both
the way grub is configured, and systemd configuration for starting up the
serial console. Note that this option must be set before installing the
operating system, it will not be modified after install.

.TP
.I \-\-dc <string>
Data center where the computer is installed.
This is seen in machine\-show or machine\-list \-a.

.TP
.I \-\-row <string>
Data center rack row where the computer is installed.
This is seen in machine\-show or machine\-list \-a.

.TP
.I \-\-rack <string>
Rack where the computer is installed.
This is seen in machine\-show or machine\-list \-a.

.TP
.I \-\-ustart <num>
Rack unit start where the computer is installed.
This is seen in machine\-show or machine\-list \-a.

.TP
.I \-\-uend <num>
Rack unit end where the computer is installed.
This is seen in machine\-show or machine\-list \-a.

.TP
.I \-\-use_ceph_if_available <yes/no>
Set a compute node to use Ceph as the backend for /var/lib/nova/instances.

.TP
.I \-\-nested\-virt <yes/no>
Set this to yes if wishing to enable nested virtualization in a compute
node.

.TP
.I \-\-use\-gpu <yes/no>
Set this to yes if the compute node is providing a GPU to its guests.

.TP
.I \-\-gpu\-name <name>
Set the name of the GPU (free string).
This option is only useful is uising \-\-use\-gpu yes.

.TP
.I \-\-gpu\-vendor\-id
Set the PCI ID of the vendor of the GPU, as seen by lspci \-nn.
This option is only useful is uising \-\-use\-gpu yes.

.TP
.I \-\-gpu\-produc\-id <ID>
Set the product ID of the GPU, as seen by lspci \-nn.
This option is only useful is uising \-\-use\-gpu yes.

.TP
.I \-\-gpu\-device\-type <type\-PCI/type\-PF/type\-VF>
Set the type of GPU to do passthrough with. Note that this depends on
the GPU card and firmware. For example, newer Nvidia Tesla T4 require
type\-PF when older firmware required type\-PCI.

.TP
.I \-\-vfio\-ids <IDs>
VFIO IDs that must be passed to the kernel module when starting. Multiple
ID strings can be passed, separated by the + caracter. A reboot (after
puppet is applied) of the compute node is necessary for this to be applied.
This option is only useful is uising \-\-use\-gpu yes.

.TP
.I \-\-cpu\-mode <host\-model/host\-passthrough/custom/cluster_value>
Set the CPU mode of a compute. With host\-passthrough, all of the compute
node CPU flags will be exposed to a guest. With custom, the cpu\-model
must be set (see below). With cluster_value, the compute node inherit from
the parameter set at the cluster level.

.TP
.I \-\-cpu\-model <MODEL>
Set the CPU model exposed to Qemu guests. The full list of supported CPU
model is to be found in the Qemu documentation. This option is

.TP
.I \-\-cpu\-model\-extra\-flags <FLAG1,FLAG2>
Extra CPU flags to pass to the guests.

.TP
.I \-\-swift\-store\-account <yes/no>
Activate Accounts storage on a swiftproxy or swiftstore node.

.TP
.I \-\-swift\-store\-container <yes/no>
Activate Containers storage on a swiftproxy or swiftstore node.

.TP
.I \-\-swift\-store\-object <yes/no>
Activate Objects storage on a swiftproxy or swiftstore node.

.TP
.I \-\-servers\-per\-port <int>
Number of servers per port for a swift Objects server.

.TP
.I \-\-disk\-chunk\-size <int>
Disk chunk size for swift objects server.

.TP
.I \-\-network\-chunk\-size <int>
Network chunk size for swift objects server.

.TP
.I \-\-lvm\-backend\-name
Set the name of the LVM backend. Only applies to Cinder volume nodes.
This can be helpful to distinguish between 2 different backend, for
example with one backend being SSD, the other one being HDD.

.TP
.I \-\-ipmi\-use <yes/no>
If set, this computer uses IPMI.

.TP
.I \-\-ipmi\-call\-chassis\-bootdev <yes/no>
If set, this computer supports using IPMI to set the boot device
(ie: disk or PXE).

.TP
.I \-\-ipmi\-username <username>
Sets the IPMI username for this computer.

After setting this value, ocicli machine-apply-ipmi must be called.

.TP
.I \-\-ipmi\-password <password>
Sets the IPMI password for this computer.

After setting this value, ocicli machine-apply-ipmi must be called.

.TP
.I \-\-ipmi\-addr <ipaddr>
Sets the IPMI address to contact the IPMI of this computer.

After setting this value, ocicli machine-apply-ipmi must be called.

.TP
.I \-\-ipmi\-port <port>
Sets the IPMI port to contact the IPMI of this computer.

After setting this value, ocicli machine-apply-ipmi must be called.

.TP
.I \-\-ipmi\-netmask <netmask>
Sets the IPMI netmask for this computer. The default value is 255.255.255.0.

After setting this value, ocicli machine-apply-ipmi must be called.

.TP
.I \-\-ipmi\-default\-gw <gatewayip>
Sets the IPMI default gateway for this computer. The default value for the
IPMI gateway is the first valid IP address of the network as per the
IPMI address and the IPMI netmask.

After setting this value, ocicli machine-apply-ipmi must be called.

.TP
.I \-\-ceph\-osd\-initial\-setup
When a machine acting as Ceph OSD, its OSDs are setup once on the first
puppet runs, and then automatically, this is disabled. This parameter
makes it possible to force the setup to be ran again, for example if
a new block device is added to the host. It is recommended to leave
this not activated in production, to handle gracefully borken block
devices.

.TP
.I \-\-force\-no\-bgp2host
If a cluster is set to use a bgp-to-the-host networking, some servers,
for example network nodes, may need to use L2 connectivity. If this
parameter is set to yes, then L2 connectivity will be forced, instead
of bgp-to-the-host over IPv6 unnumbered.

.TP
.I \-\-ipmi\-vlan <vlanid|off>
Sets the IPMI vlan. The default value for the IPMI VLAN is off, meaning
no VLAN is tagged.

After setting this value, ocicli machine-apply-ipmi must be called.

.LP
.B machine\-add
.I [\-\-fixed\-ip <ipaddr>]
.I <serial\-num> <cluster\-name> <role\-name> <location\-name>
.IP
Add a machine to a cluster. The networks attached to location specified will
automatically be assigned to that computer (ie: IP addresses will be
reserved for that machine). If it is a network node (or a controller that
acts as a network node if no network nodes are defined on the cluster) or a
compute node, if it exists, the vm\-net will also be added to the machine.
If it is a cephosd node, and a ceph network exist, it will also be added to
the machine.

When the optional parameter \-\-fixed\-ip is added, the IP address must be
in the pool that OCI will use automatically when adding the machine. The
IP must also be free.

.LP
.B machine\-remove
.I <serial\-num>
.IP
Remove a machine from a network, and make it available so it can be added
again in a cluster.

.LP
.B machine\-destroy
.I <serial\-num>
.IP
Completely remove a computer (and all of its attached network card and block
storage devices) from the OCI database. The only way to have it
re\-appear is to run the discovery agent again in the machine (which may
happen immediately if the machine is running the OCI live image).

.LP
.B machine\-reboot\-on\-hdd
.I <serial\-num>
.IP
SSH into the machine, and make it (cleanly) reboot on the HDD.

.LP
.B machine\-reboot\-on\-live
.I <serial\-num>
.IP
SSH into the machine, and make it (cleanly) reboot on the Debian live
hardware discovery system.

.LP
.B machine\-ipmi\-reboot\-on\-hdd
.I <serial\-num>
.IP
Issue an IPMI reboot command, and make the computer reboot on its installed
operating system.

.LP
.B machine\-ipmi\-reboot\-on\-live
.I <serial\-num>
.IP
Issue an IPMI reboot command, and make the computer reboot on the Debian live
hardware discovery system.

.LP
.B machine\-hpe\-ilo\-license\-install
.I <serial\-num>
.IP
If there's a /root/License.xml, this installs it as the HPE ILO license.
This file should be installed in the live image here:
/etc/openstack-cluster-installer/live-image-additions/root/License.xml

.LP
.B machine\-hpe\-activate\-ipmi\-over\-lan
.I <serial\-num>
.IP
This activates HPE IPMI over LAN.

.LP
.B machine\-ipmi\-cmd
.I <serial\-num>
.I <ipmi\-command>
.IP
Runs any IPMI command over LAN using the recorded IPMI credentials.

.LP
.B machine\-install\-os
.I <serial\-num>
.IP
Install Debian on the target computer, reboot the machine, and apply to it
its puppet configuration.

.LP
.B machine\-install\-log
.I <serial\-num>
.IP
Show the installation log of the computer, if the system is installing.
If it has rebooted under its operating system, show its puppet debug log.

.LP
.B machine\-apply\-ipmi
.I <serial\-num>
.IP
Apply the IPMI configuration to a machine (ie: run the ipmitool
commands).

.LP
.B machine\-set\-ipmi
.I [\-\-do\-ipmitool\-cmds] [\-\-ipmi\-defgw GATEWAY] [\-\-ipmi\-netmask NETMASK] [\-\-ipmi\-call\-chassis\-bootdev yes/no] <serial\-num> <use-ipmi:yes/no> <ipmi\-ip\-addr> <ipmi\-port> <ipmi\-username> <ipmi\-password>
.IP
Set the IPMI configuration of a computer.
If
.I --do-ipmitool-cmds
is used, then ipmitool commands will be issued in the target machine,
effectively setting-up the IPMI configuration.

If
.I --ipmi-call-chassis-bootdev
is used, then IPMI will be used to select the boot device before rebooting
the computers.

.LP
.B machine\-console
.I <serial\-num>
.IP
Display the ipmitool command that must be used to access the serial over LAN
of a computer, as per the information stored with the machine\-set\-ipmi.

.LP
.B machine\-wipe
.I <serial\-num>
.IP
Whipe the begining of all HDDs of the machine with /dev/urandom values.

.LP
.B machine\-ip\-list
.I <serial\-num>
.IP
Display a list of network names and IP address of a computer.

.LP
.B machine\-ip\-add
.I <serial\-num> <network\-name> <ip\-address>
.IP
Manually add an IP address to a computer. The IP must be in a range included
in the network-name.

.LP
.B machine\-ip\-remove
.I <serial\-num> <ip\-address>
.IP
Remove an IP address from a computer.

.LP
.B machine\-report\-counter\-reset
.I <serial\-num>
.IP
Reset the hardware discovery report counter. This may be useful to restart
the automated installation process.

.LP
.B machine\-renew\-ipmi\-password
.I <serial\-num>
.IP
Change the IPMI password of a server, and record it in the OCI db.

.LP
.B machine\-replace\-hardware
.I <old\-serial\-num>
.I <new\-serial\-num>
.IP
Replace a deffective machine by a new one. This assume that all hard drives
have been moved from the old machine to the new one, and that the old
hardware is being destroyed.

.LP
.B ipmi\-assign\-check
.IP
This command will check if the currently assigned IPMI address of every
machine match one of the IPMI networks, and if so, check if the IPMI
IP address is recorded in OCI's DB. If the IP address isn't there, a new
record will be added.

This command is particularly useful if migrating from an old version of OCI,
or if the administrator wishes to manually assign IPMI addresses to machines.
It's also useful simply when turning on the automatic IPMI address numbering
feature in /etc/openstack\-cluster\-installer/openstack\-cluster\-installer.conf.

.LP
.B check\-all\-ipmi [filter]
.IP
This command will iterate through all machines in the OCI database, and check if
the IPMI replies to a "chassis power status" command (within a 5 seconds
timeout).

It is possible to add a filter, matching what's in the output of ocicli
machine\-list -a. For example, it's possible to do "ocicli check\-all\-ipmi
serial LIKE \'D%\'" to check that IPMI is replying on all machines with the
serial starting by D.

.LP
.B network\-list
.IP
List all networks.

.LP
.B network\-set
.I <network\-name>
.I [\-\-cidr MASK]
.I [\-\-iface1 IFDEFINITION]
.I [\-\-iface2 IFDEFINITION]
.I [\-\-ip IPv4]
.I [\-\-is-public yes/no]
.I [\-\-location location\-name]
.I [\-\-mtu MTU]
.I [\-\-role ROLE]
.I [\-\-vlan VLAN_NUM]
.I [\-\-bridge-name NAME]
.I [\-\-ipmi\-match\-addr IPADDR]
.I [\-\-ipmi\-match\-cidr CIDR]
.IP
Set attributes of a network.

If
.I \-\-iface1
and
.I \-\-iface2
are used, bonding will be used on every machine using this network.

The
.I \-\-role
parameter can be use to set the role of the network. These roles are
currently available in OCI: ovs-bridge, ceph-cluster, vm-net and ipmi.
If not using one of these network roles, then OCI will understand that
we're talking about a machine role, and that this network should be
assigned only to the machines assigned with this role.

The
.I [\-\-mtu MTU]
and
.I [\-\-vlan VLAN_NUM]
are used to set the MTU and VLAN number of a network, during the
installation of a server. This value doesn't influence servers already
installed, which should then be tweaked manually.

The
.I [\-\-bridge-name NAME]
parameter controles the name of the birdge, in case the network role is
set to ovs-bridge.

The
.I [\-\-is-public yes/no]
is to be use on the network used to host the OpenStack API.

The
.I [\-\-ipmi\-match\-addr IPADDR]
and
.I [\-\-ipmi\-match\-cidr CIDR]
parameters are only for when the network role is set to "ipmi".
If in /etc/openstack\-cluster\-installer/openstack\-cluster\-installer.conf
the parameter automatic_ipmi_numbering=yes is set, then OCI will attempt
to match the DHCP IP address with the network described with these 2
parameters. As the default is 0.0.0.0/0, it will match by default. If there
are more than one DHCP networks, it is possible to assign different IPMI
address ranges depending on which DHCP network is matched with this rule.
For example, if you have a 2 DHCP networks, one set to PXE boot servers on
10.0.10.0/24, and that for the machines on this DHCP network, the IPMI
addresses should be on the pool 192.168.10.0/24, then this can be done:

ocicli network\-create ipmi 192.168.10.0 24 zone1 no

ocicli network-set ipmi \-\-role ipmi \-\-ipmi\-match\-addr 10.0.10.0 \-\-ipmi\-match\-cidr 24

.I [\-\-ipmi\-match\-addr IPADDR]
.I [\-\-ipmi\-match\-cidr CIDR]

.LP
.B network\-create
.I <network\-name> <ip> <cidr\-mask> <location\-name> <is\-public:yes/no>
.IP
Create a new network in the database. Note that after the network is
created, it must be added to a cluster to be effective. Note that
.I location\-name
must exist before creating the network.

.LP
.B network\-delete
.I <network\-name>
.IP
Delete a network from the db.

.LP
.B network\-add
.I <network\-name> <cluster\-name> <role\-name> <iface1> <iface2>
.IP
Add a network to a cluster.

.LP
.B network\-remove <network\-name>
.IP
Remove a network from a cluster.

.LP
.B location-list
.IP
List all location of the database.

.LP
.B location\-create
.I <location\-name> <swiftregion\-name>
.IP
Create a new location in the OCI database. Note that swiftregion\-name
before creating the location. You still need to create a swiftregion\-name
even if your cluster doesn't use swift (then the swiftregion\-name will not
be used).

.LP
.B location-delete
.I <location\-name>
.IP
Delete a location from the database. Note that no check is being done if the
record is used anywhere.

.LP
.B swift\-region\-list
.IP
List all swiftregion from the database.

.LP
.B swift\-region\-create
.I <swiftregion\-name>
.IP
Create a new Swift region.

.LP
.B swift\-region\-delete
.I <swiftregion\-name>
.IP
Delete a Swift region. Note that no check is being done if the region is
being used or not (use it at your own risk).

.LP
.B swift\-calculate\-ring
.I <cluster\-name>
.I [initial\-account\-weight [\-\-ec\-only] [initial\-container\-weight] [initial\-object\-weight]]]
.IP
Calculate the Swift ring for a new cluster. Beware that you should never
call this when a cluster has already been deployed (instead, modify the
existing ring, and distribute it to all nodes).

Eventually, one can set the initial\-weight (without it, the default weight
is 1000 for each drive).

If you wish to only create the Erasure Coding ring, add the \-\-ec\-only
parameter.

.LP
.B swift\-publish\-ring
.I <cluster\-name>
.IP
Publish a swift ring to all swiftstore and swiftproxy nodes of a cluster.
Note that this command must be issued on the OCI machine itself, as the
command isn't using any API: it just copies the local files to the cluster.

.LP
.B cluster\-list
.IP
List all clusters in the database.

.LP
.B cluster\-create
.I <cluster\-name> <example.com>
.IP
Create a new cluster with name
.I cluster\-name
and with domain name
.I example.com

.LP
.B cluster\-delete
.I <cluster\-name>
.IP
Delete a cluster form the database. Beware that no check is being done if
the cluster is still in use. Use at your own risks.

.LP
.B cluster\-show
.I <cluster\-name>
.IP
Display the information about a cluster and show its variables.

.LP
.B cluster\-set
.I <cluster\-name>
.I [\-\-time\-server\-host <time\-server\-hostname>]
.I [\-\-vip\-hostname <hostname>]
.I [\-\-swift\-part\-power <int>]
.I [\-\-swift\-proxy\-hostname <hostname>]
.I [\-\-swift\-encryption\-key\-id <UUID>]
.I [\-\-swift\-disable\-encryption <yes/no>]
.I [\-\-amp\-secgroup\-list <SECGROUP\-UUID\-LIST>]
.I [\-\-amp\-boot\-network\-list <AMP_BOOT_NETWORK_LIST>]
.I [\-\-disable\-notifications <yes/no>]
.I [\-\-enable\-monitoring\-graphs <yes/no>]
.I [\-\-monitoring\-graphite\-host <hostname>]
.I [\-\-monitoring\-graphite\-port <port>]
.I [\-\-statsd\-hostname <hostname>]
.I [\-\-extswift\-use\-external <yes/no>]
.I [\-\-extswift\-auth\-url <https://api.example.com/identity/v3>]
.I [\-\-extswift\-proxy\-url <https://prx.example.com/object/v1/AUTH_>]
.I [\-\-extswift\-project\-name <myproj>]
.I [\-\-extswift\-project\-domain\-name <default>]
.I [\-\-extswift\-user\-name <myuser>] [\-\-extswift\-use\-domain\-name <default>]
.I [\-\-extswift\-password <password>]
.I [\-\-self\-signed\-api\-cert <yes/no>]
.I [\-\-use\-ovs\-ifaces <yes/no>]
.I [\-\-cpu\-mode <host\-model/host\-passthrough/custom>]
.I [\-\-cpu\-model <MODEL>]
.I [\-\-cpu\-model\-extra\-flags <FLAG1,FLAG2>]
.I [\-\-nested\-virt <yes/no>]
.I [\-\-first\-master <SERIAL>]
.I [\-\-nameserver\-v4\-primary <IPv4>]
.I [\-\-nameserver\-v4\-secondary <IPv4>]
.I [\-\-nameserver\-v6\-primary <IPv6>]
.I [\-\-nameserver\-v6\-secondary <IPv6>]
.I [\-\-cephosd\-automatic\-provisionning <yes/no>]
.IP
Set variables for a cluster.

.TP
.I \-\-time\-server\-host <time\-server\-hostname>
Set the time server to be setup in the chrony client of each node.
Defaults to 0.debian.pool.ntp.org if not set.

.TP
.I [\-\-vip\-hostname <hostname>]
Defines the hostname of the OpenStack API if it is needed to override it.
Default to <cluster\-name>\-api.example.com.

.TP
.I [\-\-swift\-part\-power <int>]
This parameter controls the total number of partitions in the swift ring
when building the initial swift ring. The integer is a power of 2. For
example, if setting 18 (which is the default), there will be 2^18 partitions
in the ring (which is a good enough value for about 7000 disks, and it's also
small enough for smaller clusters).

.TP
.I [\-\-swift\-proxy\-hostname <hostname>]
If not set, the URL of the swift proxies will be the one of the OpenStack
API, and haproxy there will forward to the swift proxies. If you have a
busy swift cluster, then it is recommended to setup more than one swift
proxy, using a round-robin DNS entry to point to them. In such case, you
can then set the swift proxies hostname, and then clients will connect to
them directly instead of going through the HAproxy in the controllers.
With an empty string (which is the default), clients will connect through
the controller nodes.

.TP
.I [\-\-swift\-encryption\-key\-id <UUID>]
Set the barbican secret for the swift on-disk encryption. This key must
be stored in Barbican before setting this value.

.I [\-\-swift\-disable\-encryption <yes/no>]
If set to yes, there will be no on-disk encryption in the Swift cluster.
This is the default. Set the encryption key before setting-up this to no.

.TP
.I [\-\-amp\-secgroup\-list <SECGROUP\-UUID\-LIST>]
List of security groups to be assigned to Octavia amphora. These
security groups must be created before setting this value. See
the oci-octavia-amphora-secgroups-sshkey-lbrole-and-network utility
to create this.

.TP
.I [\-\-amp\-boot\-network\-list <AMP_BOOT_NETWORK_LIST>]
List of networks to use when booting-up the Octavia Amphora image.
See oci-octavia-amphora-secgroups-sshkey-lbrole-and-network utility
to create this.

.TP
.I [\-\-disable\-notifications <yes/no>]
If set to yes, there will be no notification transport set in all
of the OpenStack services. This is useful if you don't need the metrics to
be reported back to the Telemetry project (ie: Ceilometer / Gnocchi).

.TP
.I [\-\-enable\-monitoring\-graphs <yes/no>]
Enable Swift reporting of statistics.

.TP
.I [\-\-monitoring\-graphite\-host <hostname>]
Hostname of the graphite server to use in Swift nodes to report statistics.

.TP
.I [\-\-monitoring\-graphite\-port <port>]
Port number of the graphite server to use in the Swift nodes to report
statistics.

.TP
.I [\-\-statsd\-hostname <hostname>]
Hostname of the statsd server to use in the Swift nodes to report
statistics. As this improves performances, a collectd server is installed
on each nodes, which then reports to graphite. Therefore localhost is a
correct value here.

.TP
.I [\-\-extswift\-use\-external <yes/no>]
Set to yes to use an external swift for cinder-backup and glance.
Use this if you have setup a swift cluster outside of this cluster.

.TP
.I [\-\-extswift\-auth\-url <https://api.example.com/identity/v3>]
Auth URL for the external swift.

.TP
.I [\-\-extswift\-proxy\-url <https://prx.example.com/object/v1/AUTH_>]
URL of the external swift proxy.

.TP
.I [\-\-extswift\-project\-name <myproj>]
Project name of the external swift cluster.

.TP
.I [\-\-extswift\-project\-domain\-name <default>]
Project domain name of the external swift cluster.

.TP
.I [\-\-extswift\-user\-name <myuser>]
User name of the external swift cluster.

.TP
.I [\-\-extswift\-user\-domain\-name <default>]
User domain name of the external swift cluster.

.TP
.I [\-\-extswift\-password <password>]
Password of the external swift cluster.

.TP
.I [\-\-self\-signed\-api\-cert <yes/no>]
If set to yes (the default), the API URL will use a self-signed certificate.
Therefore, in all of the OpenStack configuration, there will be added some
configuration to use the ROOT CA generated by OCI when contacting the API.
This ROOT CA will also be used as environment variable by the puppet agent.

.TP
.I [\-\-use\-ovs\-ifaces <yes/no>]
If set to no (the default), the network interfaces of compute and network
nodes (and the controllers if they act as network nodes, which is the case
when no network nodes are provisioned) will use standard kernel network
interfaces. If set to yes, then a OVS bridge (and eventually an OVS bond)
will be setup instead.

.TP
.I [\-\-cpu\-mode <host\-model/host\-passthrough/custom>]
This parameter controls the CPU emulation type used in the compute nodes
of the cluster. This can be overriden on each compute node.

.TP
.I [\-\-cpu\-model <MODEL>]
This parameter controls the CPU model to use if using host\-model.

.TP
.I [\-\-cpu\-model\-extra\-flags <FLAG1,FLAG2>]
If using host\-model, it's possible to add extra CPU features to Qemu.
See the Qemu documentation or lscpu for a list of available CPU features.

.TP
.I [\-\-nested\-virt <yes/no>]
If set to yes, nested virtualization will be configured in each compute
nodes. Note that with nested virtualization in use, it is impossible to
live migrate a VM.

.TP
.I [\-\-first\-master <SERIAL>]
Set which of the 3 (or more) controllers is to be considered "first master".

While the system is fully HA, OCI still needs to have a single server that
it considers as "first master".

The "first master" of the cluster is the machine where Keystone fernet
tokens will be rotated (and then synched to other controllers), and where
the Glance API will be in used (if neither Swift or Ceph backend is
available, other nodes are only Glance API backups if that one fails).
This is also the machine where the initial database creation happens.

.TP
.I [\-\-nameserver\-v4\-primary <IPv4>]
Configure the primary name server (IPv4) for the cluster.
This is currently not in use yet and has no effect.

.TP
.I [\-\-nameserver\-v4\-secondary <IPv4>]
Configure the secondary name server (IPv4) for the cluster.
This is currently not in use yet and has no effect.

.TP
.I [\-\-nameserver\-v6\-primary <IPv6>]
Configure the primary name server (IPv6) for the cluster.
This is currently not in use yet and has no effect.

.TP
.I [\-\-nameserver\-v6\-secondary <IPv6>]
Configure the secondary name server (IPv6) for the cluster.
This is currently not in use yet and has no effect.

.TP
.I [\-\-cephosd\-automatic\-provisionning <yes/no>]
Should Ceph OSD be automatically setup after the provisionning
of the nodes?

.LP
.B cluster\-show\-networks
.I <cluster\-name>
.IP
Show all networks attached to a cluster

.LP
.B cluster\-show\-machines
.I <cluster\-name>
.IP
Show all machines attached to a cluster (ie: do not display machines
attached to another cluster, or machines not yet attached anywhere).

.LP
.B cluster\-show\-ips
.I <cluster\-name>
.IP
Display a list of IP addresses assigned in a cluster and the matching
hostname where they are assigned.

.LP
.B cluster\-install
.I <cluster\-name>
.IP
Install the system on all computers of a cluster. The installation is
made in this order: first cephmons, then cephosds, then controllers,
then network nodes, then everything else. ocicli will wait until the
controllers puppet
run is done until it installs everyting else.

Note that if for a reason, one of the steps didn't work, it's ok to
restart a cluster\-install command to finish the job, as server
install will not be attempted if a machine isn't running live.

.LP
.B cluster\-rolecounts\-list
.I <cluster\-name>
.IP
Display the current role counts for a given cluster.

.LP
.B cluster\-rolecounts\-set
.I <cluster\-name>
.I <count>
.IP
Set the role count for a given cluster and a given role.

.LP
.B cluster\-reset
.I <cluster\-name>
.IP
Reset all machines that aren't running Debian live into live mode
using IPMI commands.

.LP
.B role\-list
.IP
List all roles currently defined in OCI.

.LP
.B role\-create
.I <name>
.IP
Create a new role.

.LP
.B role\-delete
.I <name>
.IP
Delete a role.

.LP
.B filebeat\-output\-host\-list
.I <cluster\-name>
.IP
List all filebeat output hosts for a given cluster.

.LP
.B filebeat\-output\-host\-create
.I <cluster\-name>
.I <hostname>
.I <port>
.IP
Add a new filebeat output host to a cluster.

.LP
.B filebeat\-output\-host\-delete
.I <cluster\-name>
.I <hostname>
.I <port>
.IP
Delete a new filebeat output host from a cluster.

.SH AUTHORS

ocicli has been written by Thomas Goirand <zigo@debian.org>.
