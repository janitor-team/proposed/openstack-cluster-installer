define oci::oci_ssh_keypair(
  $path    = undef,
  $type    = 'ssh-rsa',
  $user    = undef,
  $group   = undef,
  $pubkey  = undef,
  $privkey = undef,
){

  $private_key_file_path = $type ? {
    'ssh-rsa'   => "${path}/.ssh/id_rsa",
    'ssh-dsa'   => "${path}/.ssh/id_dsa",
    'ssh-ecdsa' => "${path}/.ssh/id_ecdsa",
    default     => undef
  }

  file { "${path}/.ssh":
    ensure  => directory,
    mode    => '0700',
    owner   => $user,
    group   => $group,
  }

  ssh_authorized_key { "${user}-rsync-public-key":
    ensure  => present,
    key     => $pubkey,
    type    => $type,
    user    => $user,
    require => File["${path}/.ssh"],
  }

  file { $private_key_file_path:
    content => $privkey,
    mode    => '0600',
    owner   => $user,
    group   => $group,
    require => File["${path}/.ssh"],
  }
}
